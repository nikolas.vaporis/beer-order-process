An application that:

* receives (listens to) beer orders from a kafka topic
* stores them in an SQL database
* and exposes them with a REST API

An OpenAPI documentation is available.
