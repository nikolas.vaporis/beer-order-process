package org.niva.orderprocessor.kafka;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.niva.orderprocessor.kafka.dto.OrderKafkaDto;
import org.niva.orderprocessor.service.OrderService;
import org.niva.orderprocessor.util.EntitiesFactory;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@ExtendWith(MockitoExtension.class)
public class KafkaOrderListenerUnitTest {

    @InjectMocks
    private KafkaOrderListener kafkaOrderListener;

    @Mock
    private OrderService orderService;

    EntitiesFactory factory = new EntitiesFactory();

    @Test
    void listen_callsCorrectMethod() {
        OrderKafkaDto order = factory.createOrderKafkaDto();

        kafkaOrderListener.listen(order);

        verify(orderService).process(eq(order));
        verifyNoMoreInteractions(orderService);
    }
}
