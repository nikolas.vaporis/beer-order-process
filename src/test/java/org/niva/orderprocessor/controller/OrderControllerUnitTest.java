package org.niva.orderprocessor.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.niva.orderprocessor.dto.OrderDto;
import org.niva.orderprocessor.dto.OrderItemDto;
import org.niva.orderprocessor.service.OrderService;
import org.niva.orderprocessor.util.EntitiesFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OrderController.class)
class OrderControllerUnitTest {

    @InjectMocks
    OrderController orderController;

    @MockBean
    OrderService orderService;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    EntitiesFactory factory = new EntitiesFactory();

    private final String baseUri = "/api/orders";

    private String orderUri(UUID uuid) {
        return baseUri + "/" + uuid;
    }

    private String itemsUri(UUID uuid) {
        return orderUri(uuid) + "/items";
    }

    private String itemUri(UUID orderUuid, UUID itemUuid) {
        return itemsUri(orderUuid) + "/" + itemUuid;
    }

    @Test
    void listOrders() throws Exception {
        Page<OrderDto> orders = new PageImpl<>(
                Stream.generate(factory::createOrderDto).limit(6).toList());
        when(orderService.listOrders(any(Pageable.class)))
                .thenReturn(orders);

        mockMvc.perform(get(baseUri))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(orders)));

        verify(orderService).listOrders(any(Pageable.class));
        verifyNoMoreInteractions(orderService);
    }

    @Test
    void getOrder_doesNotExist_404() throws Exception {
        UUID uuid = UUID.randomUUID();
        when(orderService.findOrder(eq(uuid)))
                .thenThrow(new EntityNotFoundException());

        mockMvc.perform(get(orderUri(uuid)))
                .andExpect(status().isNotFound());

        verify(orderService).findOrder(eq(uuid));
        verifyNoMoreInteractions(orderService);
    }

    @Test
    void getOrder_exists_200() throws Exception {
        OrderDto order = factory.createOrderDto();
        UUID uuid = order.getUuid();
        when(orderService.findOrder(eq(uuid)))
                .thenReturn(order);

        mockMvc.perform(get(orderUri(uuid)))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(order)));

        verify(orderService).findOrder(eq(uuid));
        verifyNoMoreInteractions(orderService);
    }

    @Test
    void deleteOrder_doesNotExist_404() throws Exception {
        UUID uuid = UUID.randomUUID();
        doThrow(new EntityNotFoundException())
                .when(orderService).deleteOrder(eq(uuid));

        mockMvc.perform(delete(orderUri(uuid)))
                .andExpect(status().isNotFound());

        verify(orderService).deleteOrder(eq(uuid));
        verifyNoMoreInteractions(orderService);
    }

    @Test
    void deleteOrder_exists_200() throws Exception {
        UUID uuid = UUID.randomUUID();

        mockMvc.perform(delete(orderUri(uuid)))
                .andExpect(status().isNoContent());

        verify(orderService).deleteOrder(eq(uuid));
        verifyNoMoreInteractions(orderService);
    }

    @Test
    void listItems() throws Exception {
        UUID uuid = UUID.randomUUID();
        List<OrderItemDto> items = Stream.generate(factory::createOrderItemDto).limit(6).toList();
        when(orderService.listItems(eq(uuid)))
                .thenReturn(items);

        mockMvc.perform(get(itemsUri(uuid)))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(items)));

        verify(orderService).listItems(eq(uuid));
        verifyNoMoreInteractions(orderService);
    }

    @Test
    void getItem_doesNotExist_404() throws Exception {
        UUID uuid = UUID.randomUUID();
        when(orderService.findItem(eq(uuid)))
                .thenThrow(new EntityNotFoundException());

        mockMvc.perform(get(itemUri(UUID.randomUUID(), uuid)))
                .andExpect(status().isNotFound());

        verify(orderService).findItem(eq(uuid));
        verifyNoMoreInteractions(orderService);
    }

    @Test
    void getItem_exists_200() throws Exception {
        OrderItemDto item = factory.createOrderItemDto();
        UUID uuid = item.getUuid();
        when(orderService.findItem(eq(uuid)))
                .thenReturn(item);

        mockMvc.perform(get(itemUri(UUID.randomUUID(), uuid)))
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(item)));

        verify(orderService).findItem(eq(uuid));
        verifyNoMoreInteractions(orderService);
    }

    @Test
    void deleteItem_doesNotExist_404() throws Exception {
        UUID uuid = UUID.randomUUID();
        doThrow(new EntityNotFoundException())
                .when(orderService).deleteItem(eq(uuid));

        mockMvc.perform(delete(itemUri(UUID.randomUUID(), uuid)))
                .andExpect(status().isNotFound());

        verify(orderService).deleteItem(eq(uuid));
        verifyNoMoreInteractions(orderService);
    }

    @Test
    void deleteItem_exists_200() throws Exception {
        UUID uuid = UUID.randomUUID();

        mockMvc.perform(delete(itemUri(UUID.randomUUID(), uuid)))
                .andExpect(status().isNoContent());

        verify(orderService).deleteItem(eq(uuid));
        verifyNoMoreInteractions(orderService);
    }
}