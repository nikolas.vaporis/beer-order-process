package org.niva.orderprocessor.util;

import org.niva.orderprocessor.dto.OrderDto;
import org.niva.orderprocessor.dto.OrderItemDto;
import org.niva.orderprocessor.kafka.dto.BeerKafkaDto;
import org.niva.orderprocessor.kafka.dto.CustomerKafkaDto;
import org.niva.orderprocessor.kafka.dto.OrderItemKafkaDto;
import org.niva.orderprocessor.kafka.dto.OrderKafkaDto;
import org.niva.orderprocessor.model.Order;
import org.niva.orderprocessor.model.OrderItem;

import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EntitiesFactory {

    public final Random random = new Random();

    public final String CUSTOMER_NAME = "firstName lastName";

    public final String BEER_NAME = "beer name";

    public double getPrice() {
        return random.nextDouble(1, 120);
    }

    public short getQuantity() {
        return (short)random.nextInt(1, 10);
    }

    public int getNumberOfItems() {
        return random.nextInt(1, 8);
    }

    public Order createOrder() {
        return Order.builder()
                .id(random.nextInt())
                .uuid(UUID.randomUUID())
                .customer(CUSTOMER_NAME)
                .price(getPrice())
                .build();
    }

    public OrderItem createOrderItem(Order order) {
        return OrderItem.builder()
                .id(random.nextInt())
                .uuid(UUID.randomUUID())
                .beer(BEER_NAME)
                .quantity(getQuantity())
                .order(order)
                .build();
    }

    public OrderDto createOrderDto() {
        return OrderDto.builder()
                .uuid(UUID.randomUUID())
                .customer(CUSTOMER_NAME)
                .price(getPrice())
                .build();
    }

    public OrderItemDto createOrderItemDto() {
        return OrderItemDto.builder()
                .uuid(UUID.randomUUID())
                .beer(BEER_NAME)
                .quantity(getQuantity())
                .build();
    }

    public OrderKafkaDto createOrderKafkaDto() {
        return OrderKafkaDto.builder()
                .uuid(UUID.randomUUID())
                .customer(createCustomerKafkaDto())
                .items(Stream.generate(this::createOrderItemKafkaDto)
                        .limit(getNumberOfItems())
                        .collect(Collectors.toList()))
                .price(getPrice())
                .build();
    }

    private OrderItemKafkaDto createOrderItemKafkaDto() {
        return OrderItemKafkaDto.builder()
                .beer(createBeerKafkaDto())
                .quantity(getQuantity())
                .build();
    }

    public BeerKafkaDto createBeerKafkaDto() {
        return BeerKafkaDto.builder()
                .name(BEER_NAME)
                .build();
    }

    public CustomerKafkaDto createCustomerKafkaDto() {
        return CustomerKafkaDto.builder()
                .uuid(UUID.randomUUID())
                .firstName("firstName")
                .lastName("lastName")
                .phoneNumber("phoneNumber")
                .email("email")
                .address("address")
                .build();
    }
}
