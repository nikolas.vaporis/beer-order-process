package org.niva.orderprocessor.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.niva.orderprocessor.config.ModelMapperWrapper;
import org.niva.orderprocessor.dto.OrderDto;
import org.niva.orderprocessor.dto.OrderItemDto;
import org.niva.orderprocessor.kafka.dto.OrderKafkaDto;
import org.niva.orderprocessor.model.Order;
import org.niva.orderprocessor.model.OrderItem;
import org.niva.orderprocessor.repository.OrderItemRepository;
import org.niva.orderprocessor.repository.OrderRepository;
import org.niva.orderprocessor.util.EntitiesFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityNotFoundException;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderServiceUnitTest {

    @InjectMocks
    private OrderService orderService;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private OrderItemRepository orderItemRepository;

    @Spy
    private ModelMapperWrapper modelMapper;

    ArgumentCaptor<OrderItem> itemCaptor = ArgumentCaptor.forClass(OrderItem.class);

    private final EntitiesFactory entitiesFactory = new EntitiesFactory();

    @Test
    void listOrders() {
        List<Order> orders = Stream.generate(entitiesFactory::createOrder).limit(8).toList();
        when(orderRepository.findAll(any(Pageable.class)))
                .thenReturn(new PageImpl<>(orders));
        Page<OrderDto> expected = new PageImpl<>(
                orders.stream().map(modelMapper::mapToOrderDto).toList());

        Page<OrderDto> actual = orderService.listOrders(Page.empty().getPageable());

        assertEquals(expected, actual);
        verify(orderRepository).findAll(any(Pageable.class));
        verifyNoMoreInteractions(orderRepository);
        verifyNoInteractions(orderItemRepository);
    }

    @Test
    void findOrder_doesNotExist_throwException() {
        UUID uuid = UUID.randomUUID();
        when(orderRepository.findByUuid(eq(uuid)))
                .thenReturn(Optional.empty());

        assertThatThrownBy(() -> orderService.findOrder(uuid))
                .isInstanceOf(EntityNotFoundException.class);
        verify(orderRepository).findByUuid(eq(uuid));
        verifyNoMoreInteractions(orderRepository);
        verifyNoInteractions(orderItemRepository);
    }

    @Test
    void findOrder_success() {
        Order order = entitiesFactory.createOrder();
        UUID uuid = order.getUuid();
        when(orderRepository.findByUuid(eq(uuid)))
                .thenReturn(Optional.of(order));
        OrderDto expected = modelMapper.mapToOrderDto(order);

        OrderDto actual = orderService.findOrder(uuid);

        assertEquals(expected, actual);
        verify(orderRepository).findByUuid(eq(uuid));
        verifyNoMoreInteractions(orderRepository);
        verifyNoInteractions(orderItemRepository);
    }

    @Test
    void deleteOrder_doesNotExist_throwException() {
        UUID uuid = UUID.randomUUID();
        when(orderRepository.findByUuid(eq(uuid)))
                .thenReturn(Optional.empty());

        assertThatThrownBy(() -> orderService.deleteOrder(uuid))
                .isInstanceOf(EntityNotFoundException.class);

        verify(orderRepository).findByUuid(eq(uuid));
        verifyNoMoreInteractions(orderRepository);
        verifyNoInteractions(orderItemRepository);
    }

    @Test
    void deleteOrder_exists() {
        Order order = entitiesFactory.createOrder();
        UUID uuid = order.getUuid();
        when(orderRepository.findByUuid(eq(uuid)))
                .thenReturn(Optional.of(order));
        List<OrderItem> items = Stream.generate(() -> entitiesFactory.createOrderItem(null)).limit(8).toList();
        when(orderItemRepository.findByOrderUuid(eq(uuid)))
                .thenReturn(items);

        orderService.deleteOrder(uuid);

        verify(orderRepository).findByUuid(eq(uuid));
        verify(orderRepository).deleteByUuid(eq(uuid));
        verifyNoMoreInteractions(orderRepository);
        verify(orderItemRepository).findByOrderUuid(eq(uuid));
        verify(orderItemRepository).deleteAll(eq(items));
        verifyNoMoreInteractions(orderItemRepository);
    }

    @Test
    void listItems() {
        UUID orderUuid = UUID.randomUUID();
        List<OrderItem> items = Stream.generate(() -> entitiesFactory.createOrderItem(null)).limit(8).toList();
        when(orderItemRepository.findByOrderUuid(eq(orderUuid)))
                .thenReturn(items);
        List<OrderItemDto> expected = items.stream().map(modelMapper::mapToOrderItemDto).toList();

        List<OrderItemDto> actual = orderService.listItems(orderUuid);

        assertEquals(expected, actual);
        verify(orderItemRepository).findByOrderUuid(eq(orderUuid));
        verifyNoMoreInteractions(orderRepository);
        verifyNoInteractions(orderRepository);
    }

    @Test
    void findItem_doesNotExist_throwException() {
        UUID uuid = UUID.randomUUID();
        when(orderItemRepository.findByUuid(eq(uuid)))
                .thenReturn(Optional.empty());

        assertThatThrownBy(() -> orderService.findItem(uuid))
                .isInstanceOf(EntityNotFoundException.class);
        verify(orderItemRepository).findByUuid(eq(uuid));
        verifyNoMoreInteractions(orderItemRepository);
        verifyNoInteractions(orderRepository);
    }

    @Test
    void findItem_success() {
        OrderItem orderItem = entitiesFactory.createOrderItem(null);
        UUID uuid = orderItem.getUuid();
        when(orderItemRepository.findByUuid(eq(uuid)))
                .thenReturn(Optional.of(orderItem));
        OrderItemDto expected = modelMapper.mapToOrderItemDto(orderItem);

        OrderItemDto actual = orderService.findItem(uuid);

        assertEquals(expected, actual);
        verify(orderItemRepository).findByUuid(eq(uuid));
        verifyNoMoreInteractions(orderItemRepository);
        verifyNoInteractions(orderRepository);
    }

    @Test
    void deleteItem_doesNotExist_throwException() {
        UUID uuid = UUID.randomUUID();
        when(orderItemRepository.deleteByUuid(eq(uuid)))
                .thenReturn(0);

        assertThatThrownBy(() -> orderService.deleteItem(uuid))
                .isInstanceOf(EntityNotFoundException.class);

        verify(orderItemRepository).deleteByUuid(eq(uuid));
        verifyNoMoreInteractions(orderItemRepository);
        verifyNoInteractions(orderRepository);
    }

    @Test
    void deleteItem_exists() {
        UUID uuid = UUID.randomUUID();
        when(orderItemRepository.deleteByUuid(eq(uuid)))
                .thenReturn(1);

        orderService.deleteItem(uuid);

        verify(orderItemRepository).deleteByUuid(eq(uuid));
        verifyNoMoreInteractions(orderItemRepository);
        verifyNoInteractions(orderRepository);
    }

    @Test
    void process_nullUuid_noInteractionWithRepositories() {
        OrderKafkaDto kafkaDto = entitiesFactory.createOrderKafkaDto()
                .setUuid(null);

        orderService.process(kafkaDto);

        verifyNoInteractions(orderRepository);
        verifyNoInteractions(orderItemRepository);
    }

    @Test
    void process_nullCustomer_noInteractionWithRepositories() {
        OrderKafkaDto kafkaDto = entitiesFactory.createOrderKafkaDto()
                .setCustomer(null);

        orderService.process(kafkaDto);

        verifyNoInteractions(orderRepository);
        verifyNoInteractions(orderItemRepository);
    }

    @Test
    void process_nullItems_noInteractionWithRepositories() {
        OrderKafkaDto kafkaDto = entitiesFactory.createOrderKafkaDto()
                .setItems(null);

        orderService.process(kafkaDto);

        verifyNoInteractions(orderRepository);
        verifyNoInteractions(orderItemRepository);
    }

    @Test
    void process_emptyListOfItems_noInteractionWithRepositories() {
        OrderKafkaDto kafkaDto = entitiesFactory.createOrderKafkaDto()
                .setItems(List.of());

        orderService.process(kafkaDto);

        verifyNoInteractions(orderRepository);
        verifyNoInteractions(orderItemRepository);
    }

    @Test
    void process_success() {
        OrderKafkaDto orderKafkaDto = entitiesFactory.createOrderKafkaDto();
        Order order = modelMapper.mapToOrder(orderKafkaDto);
        when(orderRepository.save(eq(order)))
                .thenReturn(order);
        List<OrderItem> items = orderKafkaDto.getItems().stream()
                .map(modelMapper::mapToOrderItem)
                .toList();

        orderService.process(orderKafkaDto);

        verify(orderRepository).save(eq(order));
        verifyNoMoreInteractions(orderRepository);
        verify(orderItemRepository, times(items.size())).save(itemCaptor.capture());
        assertItems(items, itemCaptor.getAllValues(), order);
        verifyNoMoreInteractions(orderItemRepository);
    }

    private void assertItems(List<OrderItem> items, List<OrderItem> capturedItems, Order order) {
        assertEquals(items.size(), capturedItems.size());
        Iterator<OrderItem> itemIterator = items.iterator();
        for (OrderItem capturedItem : capturedItems) {
            assertNotNull(capturedItem.getUuid());
            assertEquals(order, capturedItem.getOrder());
            OrderItem item = itemIterator.next();
            assertEquals(item.getBeer(), capturedItem.getBeer());
            assertEquals(item.getQuantity(), capturedItem.getQuantity());
        }
    }
}