package org.niva.orderprocessor.config;

import lombok.Getter;
import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;
import org.niva.orderprocessor.dto.OrderDto;
import org.niva.orderprocessor.dto.OrderItemDto;
import org.niva.orderprocessor.kafka.dto.BeerKafkaDto;
import org.niva.orderprocessor.kafka.dto.OrderItemKafkaDto;
import org.niva.orderprocessor.kafka.dto.OrderKafkaDto;
import org.niva.orderprocessor.model.Order;
import org.niva.orderprocessor.model.OrderItem;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class ModelMapperWrapper {

    @Getter
    private final ModelMapper modelMapper;

    public ModelMapperWrapper() {
        this.modelMapper = new ModelMapper();
        modelMapper.typeMap(OrderItemKafkaDto.class, OrderItem.class).addMappings(mapper -> mapper
                .using(ctx -> ((BeerKafkaDto)ctx.getSource()).getName())
                .map(OrderItemKafkaDto::getBeer, OrderItem::setBeer)
        );
        modelMapper.addConverter(new OrderConverter());
    }

    private static class OrderConverter extends AbstractConverter<OrderKafkaDto, Order> {
        @Override
        protected Order convert(OrderKafkaDto dto) {
            return Order.builder()
                    .uuid(dto.getUuid())
                    .customer(dto.getCustomer().getFirstName() + " " + dto.getCustomer().getLastName())
                    .price(dto.getPrice())
                    .build();
        }
    }

    public OrderDto mapToOrderDto(Order entity) {
        return modelMapper.map(entity, OrderDto.class);
    }

    public OrderItemDto mapToOrderItemDto(OrderItem entity) {
        return modelMapper.map(entity, OrderItemDto.class);
    }

    public Order mapToOrder(OrderKafkaDto dto) {
        return modelMapper.map(dto, Order.class);
    }

    public OrderItem mapToOrderItem(OrderItemKafkaDto dto) {
        return modelMapper.map(dto, OrderItem.class);
    }
}
