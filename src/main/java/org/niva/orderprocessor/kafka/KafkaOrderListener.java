package org.niva.orderprocessor.kafka;

import org.niva.orderprocessor.kafka.dto.OrderKafkaDto;
import org.niva.orderprocessor.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaOrderListener {

    @Autowired
    private OrderService orderService;

    @KafkaListener(topics = "#{kafkaConfigurationProperties.getOrderTopic()}",
                  groupId = "#{kafkaConfigurationProperties.getOrderGroupId()}")
    public void listen(OrderKafkaDto order) {
        orderService.process(order);
    }
}
