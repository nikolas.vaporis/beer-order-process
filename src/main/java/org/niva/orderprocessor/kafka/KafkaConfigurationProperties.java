package org.niva.orderprocessor.kafka;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("kafka")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KafkaConfigurationProperties {

    private String bootstrapServer;
    private String orderTopic;
    private String orderGroupId;
}
