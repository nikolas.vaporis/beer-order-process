package org.niva.orderprocessor.kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.niva.orderprocessor.kafka.dto.OrderKafkaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.Map;

@Configuration
public class KafkaConsumerConfig {

    @Autowired
    KafkaConfigurationProperties properties;

    @Bean
    public ConsumerFactory<String, OrderKafkaDto> consumerFactory() {
        return new DefaultKafkaConsumerFactory<>(
                Map.of(
                        ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, properties.getBootstrapServer(),
                        ConsumerConfig.GROUP_ID_CONFIG, properties.getOrderGroupId()),
                new StringDeserializer(),
                new JsonDeserializer<>(OrderKafkaDto.class, false));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, OrderKafkaDto> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, OrderKafkaDto> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }
}
