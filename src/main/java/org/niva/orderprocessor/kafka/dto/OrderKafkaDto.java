package org.niva.orderprocessor.kafka.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OrderKafkaDto {

    private UUID uuid;
    private CustomerKafkaDto customer;
    private List<OrderItemKafkaDto> items;
    private double price;
}
