package org.niva.orderprocessor.service;

import org.niva.orderprocessor.config.ModelMapperWrapper;
import org.niva.orderprocessor.dto.OrderDto;
import org.niva.orderprocessor.dto.OrderItemDto;
import org.niva.orderprocessor.kafka.dto.OrderItemKafkaDto;
import org.niva.orderprocessor.kafka.dto.OrderKafkaDto;
import org.niva.orderprocessor.model.Order;
import org.niva.orderprocessor.model.OrderItem;
import org.niva.orderprocessor.repository.OrderItemRepository;
import org.niva.orderprocessor.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.UUID;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    ModelMapperWrapper modelMapper;

    public Page<OrderDto> listOrders(Pageable pageable) {
        return orderRepository.findAll(pageable)
                .map(modelMapper::mapToOrderDto);
    }

    public OrderDto findOrder(UUID uuid) {
        return orderRepository.findByUuid(uuid)
                .map(modelMapper::mapToOrderDto)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Transactional
    public void deleteOrder(UUID uuid) {
        if (orderRepository.findByUuid(uuid).isEmpty()) {
            throw new EntityNotFoundException();
        }
        orderItemRepository.deleteAll(orderItemRepository.findByOrderUuid(uuid));
        orderRepository.deleteByUuid(uuid);
    }

    public List<OrderItemDto> listItems(UUID uuid) {
        return orderItemRepository.findByOrderUuid(uuid)
                .stream().map(modelMapper::mapToOrderItemDto)
                .toList();
    }

    public OrderItemDto findItem(UUID uuid) {
        return orderItemRepository.findByUuid(uuid)
                .map(modelMapper::mapToOrderItemDto)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Transactional
    public void deleteItem(UUID uuid) {
        if (orderItemRepository.deleteByUuid(uuid) == 0) {
            throw new EntityNotFoundException();
        }
    }

    public void process(OrderKafkaDto kafkaDto) {
        if (validate(kafkaDto)) {
            Order order = saveOrder(kafkaDto);
            kafkaDto.getItems().forEach(item -> fillAndSaveItem(item, order));
        }
    }

    private boolean validate(OrderKafkaDto kafkaDto) {
        return kafkaDto.getUuid() != null
                && kafkaDto.getCustomer() != null
                && kafkaDto.getItems() != null
                && !kafkaDto.getItems().isEmpty();
    }

    @Transactional
    private Order saveOrder(OrderKafkaDto kafkaDto) {
        return orderRepository.save(modelMapper.mapToOrder(kafkaDto));
    }

    @Transactional
    private void fillAndSaveItem(OrderItemKafkaDto itemKafkaDto, Order order) {
        OrderItem orderItem = modelMapper.mapToOrderItem(itemKafkaDto)
                .setUuid(UUID.randomUUID())
                .setOrder(order);
        orderItemRepository.save(orderItem);
    }
}
