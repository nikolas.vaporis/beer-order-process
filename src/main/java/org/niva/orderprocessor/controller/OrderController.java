package org.niva.orderprocessor.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.niva.orderprocessor.dto.OrderDto;
import org.niva.orderprocessor.dto.OrderItemDto;
import org.niva.orderprocessor.service.OrderService;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Operation(summary = "Get a page of all orders")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "a page of all orders has been retrieved",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = OrderDto.class)) }),
            @ApiResponse(responseCode = "400", description = "invalid sorting parameter", content = @Content)})
    @GetMapping
    public Page<OrderDto> listOrders(@ParameterObject Pageable pageable) {
        return orderService.listOrders(pageable);
    }

    @Operation(summary = "Get an order by its uuid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the order", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = OrderDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid uuid supplied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Order not found", content = @Content) })
    @GetMapping("/{uuid}")
    public OrderDto getOrder(@Parameter(description = "uuid of order to be searched") @PathVariable UUID uuid) {
        return orderService.findOrder(uuid);
    }

    @Operation(summary = "Delete an order by its uuid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Order has been deleted", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = OrderDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid uuid supplied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Order not found", content = @Content) })
    @DeleteMapping("/{uuid}")
    public ResponseEntity<Void> deleteOrder(@Parameter(description = "uuid of order to be deleted") @PathVariable UUID uuid) {
        orderService.deleteOrder(uuid);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get a page of all order-items")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "a page of all order-items has been retrieved",
                    content = { @Content(mediaType = "application/json", schema = @Schema(implementation = OrderItemDto.class)) }),
            @ApiResponse(responseCode = "400", description = "invalid sorting parameter", content = @Content)})
    @GetMapping("/{uuid}/items")
    public List<OrderItemDto> listItems(@Parameter(description = "uuid of order") @PathVariable UUID uuid) {
        return orderService.listItems(uuid);
    }

    @Operation(summary = "Get an order-item by its uuid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the order-item", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = OrderItemDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid uuid supplied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Order-item not found", content = @Content) })
    @GetMapping("/{orderUuid}/items/{itemUuid}")
    public OrderItemDto getItem(@Parameter(description = "uuid of order-item to be searched") @PathVariable UUID itemUuid) {
        return orderService.findItem(itemUuid);
    }

    @Operation(summary = "Delete an order-item by its uuid")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Order-item has been deleted", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = OrderItemDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid uuid supplied", content = @Content),
            @ApiResponse(responseCode = "404", description = "Order-item not found", content = @Content) })
    @DeleteMapping("/{orderUuid}/items/{itemUuid}")
    public ResponseEntity<Void> deleteItem(@Parameter(description = "uuid of order-item to be deleted") @PathVariable UUID itemUuid) {
        orderService.deleteItem(itemUuid);
        return ResponseEntity.noContent().build();
    }
}
