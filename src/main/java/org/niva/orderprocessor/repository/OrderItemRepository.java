package org.niva.orderprocessor.repository;

import org.niva.orderprocessor.model.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OrderItemRepository extends JpaRepository<OrderItem, Integer> {

    Optional<OrderItem> findByUuid(UUID uuid);

    @Query("select i from OrderItem i where i.order = (select o from Order o where o.uuid = :orderUuid)")
    List<OrderItem> findByOrderUuid(UUID orderUuid);

    int deleteByUuid(UUID uuid);
}