package org.niva.orderprocessor.repository;

import org.niva.orderprocessor.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, Integer> {

    Optional<Order> findByUuid(UUID uuid);

    int deleteByUuid(UUID uuid);
}